import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class RoutesReducer extends MapReduceBase implements Reducer <Text, Text, Text, Text> {

	public void configure(JobConf arg0) {
		// TODO Auto-generated method stub
		
	}

	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void reduce(Text key, Iterator<Text> val, OutputCollector<Text, Text> output, Reporter arg3)
			throws IOException {
		// TODO Auto-generated method stub

		StringBuffer sb= new StringBuffer();
		
		int trip_Count=0;
		Double sum=0.0d;
		Double avg=0.0d;
		while(val.hasNext())
		{
			String s= val.next().toString();
			sum=sum+Double.parseDouble(s);
			trip_Count++;
		}
		avg=sum/trip_Count;
		String fop= ","+trip_Count+","+avg;
		
		output.collect(key, new Text(fop));
		
	}

}
