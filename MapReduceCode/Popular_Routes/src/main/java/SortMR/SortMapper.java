package SortMR;

import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class SortMapper implements Mapper<LongWritable, Text, IntWritable, Text> {

	String prefix = null;
	boolean flag = false;

	public void configure(JobConf arg0) {
		// TODO Auto-generated method stub

	}

	public void close() throws IOException {
		// TODO Auto-generated method stub

	}

	public void map(LongWritable lineno, Text text, OutputCollector<IntWritable, Text> output, Reporter reporter)
			throws IOException {
		// TODO Auto-generated method stub
		// skip the first line
		String s = text.toString();

		String key = s.substring(s.indexOf(",")+1, s.lastIndexOf(","));
		int i = (Integer.parseInt(key) * (-1));
		IntWritable k = new IntWritable(i);

		String slong = s.substring(0, s.indexOf(":"));
		String slat = s.substring(s.indexOf(":")+1, s.indexOf("->"));
		String dlong = s.substring(s.indexOf("->") + 2, s.lastIndexOf(":"));
		String dlat = s.substring(s.lastIndexOf(":")+1, s.indexOf(",")).trim();
		String avgtip = s.substring(s.lastIndexOf(","), s.length());

		String op = ","+slong + "," + slat + "," + dlong + "," + dlat + "," + avgtip + "";

		output.collect(k, new Text(op));

	}

}
