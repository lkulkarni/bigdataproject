import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class RoutesMapper implements Mapper<LongWritable, Text, Text, Text> {

	String prefix = null;
	boolean flag = false;

	public void configure(JobConf arg0) {
		// TODO Auto-generated method stub

	}

	public void close() throws IOException {
		// TODO Auto-generated method stub

	}

	public void map(LongWritable lineno, Text text, OutputCollector<Text, Text> output, Reporter reporter)
			throws IOException {
		// TODO Auto-generated method stub
				// skip the first line
		String s = text.toString();

		if (!s.startsWith("VendorID")) {
			String[] line_parts = s.split(",");

			String slong=Helper.getCoor(line_parts[4]);
			String slat=Helper.getCoor(line_parts[5]);
			String dlong=Helper.getCoor(line_parts[6]);
			String dlat=Helper.getCoor(line_parts[7]);
			
			String key=slong+":"+slat+"->"+ dlong+":"+dlat;
			String tip=line_parts[12];
			
			output.collect(new Text(key), new Text(tip));
			
		}
		
		

	}

}
