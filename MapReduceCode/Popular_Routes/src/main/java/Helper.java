import java.text.DecimalFormat;

public class Helper{
	
	public static String getCoor(String coor)
	{
		String opcoor=null;
		Double d = Double.parseDouble(coor);
		DecimalFormat newFormat= new DecimalFormat("###.###"); 
		d= Double.valueOf(newFormat.format(d));
		opcoor=d.toString();
		
		return opcoor;
	}
	
}