import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringTest {
	
	public static void main(String args[])
	{
		String line="25\",\"11:51 PM,33.1,18.0,54,30.27,10.0,NE,8.1,-,N/A,,Overcast,50,2014-01-02 04:51:00<br />";
		
		System.out.println(line.substring(line.indexOf(",\"")+2, line.lastIndexOf("<br />")));
		
		
		Pattern p= Pattern.compile("[0-9]{4}[\\-]{1}[0-9]{2}[\\-][0-9]{2}");
		
		Matcher m= p.matcher(line);
		
		System.out.println(m.find()+ m.group());
		
		
	}

}
