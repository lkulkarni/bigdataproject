import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {

	public static String getDateFromLine(String line) {

		Pattern p = Pattern.compile("[0-9]{4}[\\-]{1}[0-9]{2}[\\-][0-9]{2}");

		Matcher m = p.matcher(line);

		return m.group();

	}

	public static String convertToDate(String date) throws ParseException {

		SimpleDateFormat inputF = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		SimpleDateFormat outputF = new SimpleDateFormat("yyyy-MM-dd HH");
		Date d = inputF.parse(date);
		String output = outputF.format(d);

		return output;
	}

}
