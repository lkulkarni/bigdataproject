import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class WeatherMapper implements Mapper<LongWritable, Text, Text, Text> {

	String prefix = null;
	boolean flag = false;

	public void configure(JobConf arg0) {
		// TODO Auto-generated method stub

	}

	public void close() throws IOException {
		// TODO Auto-generated method stub

	}

	public void map(LongWritable lineno, Text text, OutputCollector<Text, Text> output, Reporter reporter)
			throws IOException {
		// TODO Auto-generated method stub
		String skip = "\"\",\"TimeEST.TemperatureF.Dew.PointF.Humidity.Sea.Level.PressureIn.VisibilityMPH.Wind.Direction.Wind.SpeedMPH.Gust.SpeedMPH.PrecipitationIn.Events.Conditions.WindDirDegrees.DateUTC.br...\"";
		// skip the first line
		String s = text.toString();

		if (!s.startsWith("\"\",\"")) {
			String line = text.toString();

			String[] line_parts = line.substring(line.indexOf(",\"") + 2, line.indexOf("<br />")).split(",");

			if (flag == false) {
				prefix = line_parts[13].substring(0, 10) + " ";
				flag = true;
			}

			String tempKey = prefix + line_parts[0];

			String opkey = null;
			try {
				opkey = Helper.convertToDate(tempKey);
			} catch (Exception e) {

				e.printStackTrace();
			}
			String opval = line_parts[1] + "," + line_parts[11];

			output.collect(new Text(opkey), new Text(opval));

		}

	}

}
